var express = require("express");
var router = express.Router();
var { verify, getSession } = require("../helpers/db");

router.get("/", async function (req, res, next) {
  let database = "down";
  try {
    let status = await verify();
    if (status) database = "up";
  } catch (e) {}

  res.send({ application: "up", database });
});

router.get("/db", async function (req, res) {
  const session = getSession();
  try {
    const result = await session.run("MATCH (a:Entity) RETURN count(a)");

    const record = result.records[0];
    const count = record.get(record.keys[0]);
    res.send({ count });
  } finally {
    await session.close();
  }
});

router.get("/stats", async function (req, res) {
  const session = getSession();

  const results = await session.run("CALL db.schema.visualization()");

  const record = results.records[0];
  const nodes = record.get("nodes");
  const relationships = record.get("relationships");

  nodes.forEach((n) => {
    n.identity = n.identity.toInt();
  });
  relationships.forEach((r) => {
    r.identity = r.identity.toInt();
    r.start = r.start.toInt();
    r.end = r.end.toInt();
  });

  res.send({ nodes, relationships });
});

module.exports = router;
