const neo4j = require("neo4j-driver");

const authToken = neo4j.auth.basic(process.env.DB_USER, process.env.DB_PASS);
const driver = neo4j.driver(process.env.DB_HOST, authToken);

function getSession() {
  return driver.session();
}

function verify() {
  return driver.verifyConnectivity();
}

module.exports = { verify, getSession };
