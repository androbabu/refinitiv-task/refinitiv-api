## Getting Started

This applications provides api to visualize the overview of graph nodes and it's relationship.

## Local Development

Create `.env` file by copying from the `.env.example` file and update the required values

Execute `yarn start` to start the local server

## Deployment

The project is built in Gitlab CI and will be deployed to Heroku.

Deployed Link: [https://refinitiv-offshore-leaks.herokuapp.com/](https://refinitiv-offshore-leaks.herokuapp.com/)
